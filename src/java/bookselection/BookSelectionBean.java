package bookselection;

import java.io.Serializable;
import java.util.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Simple ScopedBean storing information about selected books
 * @author kasiabm
 * @version 29/05/2015
 */
@ManagedBean (name="bookSelectionBean")
@SessionScoped
public class BookSelectionBean implements Serializable {
    //instance variables
    //book titles
    private static final HashMap<String, String> books = new HashMap(); 
    //book prices
    private static final HashMap<String, Double> prices = new HashMap();
    //stores user's selections
    private Map<String, Double> cart = new HashMap();
    //stores user's current selection
    private String book;
    //stores current price
    private double price;
    //stores cart's total value
    private double totalPrice = 0.0;
    //stores totalPrice in String object
    private String totPrice;
    
    //initialises books and prices
    static {
        books.put("scala", "Programming in Scala");
        books.put("python", "Learning Python");
        books.put("java", "Java: How to Program");
        books.put("android", "Android: How to Program");
        books.put("ruby", "Beginning Ruby");
        books.put("cpp", "C++: How to Program");
        prices.put("scala", 57.95);
        prices.put("python", 36.86);
        prices.put("java", 65.90);
        prices.put("android", 45.30);
        prices.put("ruby", 47.99);
        prices.put("cpp", 38.17);
    } //end constructor
        
    //stores user's selection
    public void setBook(String aBook) {
        book = books.get(aBook);
        price = prices.get(aBook);
        if (!cart.containsKey(aBook)) {
            cart.put(book, price);
            totalPrice += price; 
        }
    } //end method setBook
    
    //returns the cart size
    public int getCartSize() {
        return cart.size();
    } //end method getCartSize
    
    //returns current selection
    public String getBook() {
        return book;
    } //end method getBook
       
    //returns book price
    public double getPrice() {
        return price;
    } //end method getPrice
    
    //returns total cart price
    public double getTotalPrice() {
        return totalPrice;
    } //end method getTotalPrice
    
    //returns String representation of total cart price
    public String getTotPrice() {
        return String.format("%.2f", totalPrice);
    } //end method getTotPrice
  
    //returns the cart content
    public List getCart() {
        List content = new ArrayList();
        for (String aBook : cart.keySet()) {
            String aTitle = aBook;
            double aPrice = cart.get(aBook);
            String addBook = String.format("%s - £%.2f", aTitle, aPrice);
            content.add(addBook);
        }
        return content;
    } //end method getCart
} //end class BookSelectionBean